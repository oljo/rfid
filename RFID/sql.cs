﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Data;
using System.Text;
using System.Threading.Tasks;
using MySql.Data;
using System.Windows;
using MySql.Data.MySqlClient;
using System.Windows.Controls;
using Newtonsoft.Json;

using System.Net;
using System.Windows.Media.Imaging;


namespace RFID
{
    class sql
    {
        /*           SQL queries:
         *  Actors in film: SELECT name FROM `film` JOIN(film_actor) ON (film.id_film=film_actor.id_film) JOIN actor ON actor.id_actor=film_actor.id_actor WHERE film.id_film= group by name
         *  Director in film: SELECT name FROM `film` JOIN(film_director) ON (film.id_film=film_director.id_film) JOIN director ON director.id_director=film_director.id_director WHERE film.id_film= group by name
         *  Writers in film: SELECT name FROM `film` JOIN(film_writer) ON (film.id_film=film_writer.id_film) JOIN writer ON writer.id_writer=film_writer.id_writer WHERE film.id_film= group by name
         *  Film categories: SELECT name FROM `film` JOIN(film_category) ON (film.id_film=film_category.id_film) JOIN category ON category.id_category=film_category.id_category WHERE film.id_film= group by name
         * 
         * 
         * 
         * 
         * */

        public MySql.Data.MySqlClient.MySqlConnection conn;

        public enum movieData
        {
            category,
            title,
            id,
            quality,
            subtitles,
            plot,
            length,
            rating,
            year,
            actors,
            writers,
            directors,

        }

        public sql(string sql_server, string uid, string pwd, string database)
        {
            string connectSql = "SERVER=" + sql_server + ";" + "DATABASE=" +
            database + ";" + "UID=" + uid + ";" + "PASSWORD=" + pwd + ";";
            conn = new MySql.Data.MySqlClient.MySqlConnection(connectSql);
            openConnection();
            conn.Close();
        }

        public Boolean insert_movie(string title, int year, int quality)
        {

            int checkMovie = -1;
            using (var checkMovie_cmd = new MySqlCommand("SELECT id_film from film where title=@movieTitle AND id_quality=@idQuality",
                              conn))
            {
                openConnection();
                checkMovie_cmd.Parameters.Add("@movieTitle", MySqlDbType.VarChar).Value = title;
                checkMovie_cmd.Parameters.Add("@idQuality", MySqlDbType.Int16).Value = quality;
                try { checkMovie = (int)checkMovie_cmd.ExecuteScalar(); }
                catch (NullReferenceException e) { checkMovie = -1; }
                catch (MySqlException e) { checkMovie = -1; }
                conn.Close();
            }

            if (checkMovie == -1)
            {
                omdb_api omdb = new omdb_api();
                var movie_info = omdb.dataRequest(title, year);

                if (movie_info == null)//elokuvaa ei löytynyt
                {
                    conn.Close();
                    return false;
                }
                /*{"Title":"Pulp Fiction","Year":"1994",
                "Rated":"R","Released":"14 Oct 1994",
                "Runtime":"154 min","Genre":"Crime, Drama, Thriller",
                "Director":"Quentin Tarantino",
                "Writer":"Quentin Tarantino (story), Roger Avary (story), Quentin Tarantino",
                "Actors":"Tim Roth, Amanda Plummer, Laura Lovelace, John Travolta",
                "Plot":"Jules Winnfield and Vincent Vega are two hitmen who are out to retrieve a suitcase stolen from their employer, mob boss Marsellus Wallace. Wallace has also asked Vincent to take his wife Mia out a few days later when Wallace himself will be out of town. Butch Coolidge is an aging boxer who is paid by Wallace to lose his next fight. The lives of these seemingly unrelated people are woven together comprising of a series of funny, bizarre and uncalled-for incidents.",*
                "Language":"English, Spanish, French","Country":"USA","Awards":"Won 1 Oscar. Another 63 wins & 47 nominations.",
                "Poster":"http://ia.media-imdb.com/images/M/MV5BMjE0ODk2NjczOV5BMl5BanBnXkFtZTYwNDQ0NDg4._V1_SX300.jpg",
                "Metascore":"94","imdbRating":"8.9","imdbVotes":"1,039,031","imdbID":"tt0110912","Type":"movie","Response":"True"} */
                else
                {
                    //int idDir = directorCheck(movie_info["Director"].ToString());

                    string[] actors = csv_parser(movie_info["Actors"].ToString());
                    string[] languages = csv_parser(movie_info["Language"].ToString());
                    string[] writers = csv_parser(movie_info["Writer"].ToString());
                    string[] directors = csv_parser(movie_info["Director"].ToString());
                    string[] categories = csv_parser(movie_info["Genre"].ToString());

                    int[] idActors = new int[actors.Length];
                    int[] idDirectors = new int[directors.Length];
                    int[] idWriters = new int[writers.Length];
                    int[] idLanguages = new int[languages.Length];
                    int[] idCategories = new int[categories.Length];

                    string ttl = movie_info["Title"].ToString();

                    for (int i = 0; i < actors.Length; i++)
                    {
                        idActors[i] = actorCheck(actors[i]);
                    }

                    for (int i = 0; i < writers.Length; i++)
                    {
                        idWriters[i] = writerCheck(writers[i]);
                    }


                    for (int i = 0; i < directors.Length; i++)
                    {
                        idDirectors[i] = directorCheck(directors[i]);
                    }



                    for (int i = 0; i < categories.Length; i++)
                    {
                        idCategories[i] = categoryCheck(categories[i]);
                    }

                    for (int i = 0; i < languages.Length; i++)
                    {
                        idLanguages[i] = languageCheck(languages[i]);
                    }


                    using (var add_film_cmd = new MySqlCommand("INSERT INTO film(id_language,id_quality,id_subtitles,description,length,rating,year,title,poster) values(@id_language,@id_quality,@id_subtitles,@description,@length,@rating,@release_year,@title,@posterdata)",
                      conn))
                    {

                        //values(@id_category,@id_language,@id_quality,@id_subtitles,@description,@length,@rating,@release_year,@title)",
                        openConnection();
                        try
                        {
                            int yearIn;
                            int.TryParse(movie_info["Year"].ToString(), out yearIn);

                            Byte[] posterData = null;
                            if (movie_info["Poster"].ToString() != "N/A")
                            {
                                posterData = get_posterFromURI(movie_info["Poster"].ToString());
                            }



                            add_film_cmd.Parameters.Add("@description", MySqlDbType.VarChar).Value = movie_info["Plot"].ToString();

                            add_film_cmd.Parameters.Add("@id_quality", MySqlDbType.VarChar).Value = quality;
                            add_film_cmd.Parameters.Add("@id_subtitles", MySqlDbType.Int16).Value = 1;
                            add_film_cmd.Parameters.Add("@id_language", MySqlDbType.Int16).Value = idLanguages[0];

                            add_film_cmd.Parameters.Add("@length", MySqlDbType.VarChar).Value = movie_info["Runtime"].ToString();
                            add_film_cmd.Parameters.Add("@rating", MySqlDbType.Float).Value = movie_info["imdbRating"].ToString();
                            add_film_cmd.Parameters.Add("@release_year", MySqlDbType.Year).Value = yearIn;
                            add_film_cmd.Parameters.Add("@title", MySqlDbType.VarChar).Value = movie_info["Title"].ToString();

                            add_film_cmd.Parameters.Add("@posterdata", MySqlDbType.LongBlob).Value = posterData;
                            posterData = null;

                            add_film_cmd.ExecuteNonQuery();
                        }
                        catch (MySqlException e) { MessageBox.Show(e.ToString()); }
                        catch (FormatException e) { MessageBox.Show(e.ToString()); }
                    }
                    using (var checkMovie_cmd = new MySqlCommand("SELECT id_film from film where title=@movieTitle AND id_quality=@idQuality",
                  conn))
                    {

                        checkMovie_cmd.Parameters.Add("@movieTitle", MySqlDbType.VarChar).Value = title;
                        checkMovie_cmd.Parameters.Add("@idQuality", MySqlDbType.Int16).Value = quality;
                        try { checkMovie = (int)checkMovie_cmd.ExecuteScalar(); }
                        catch (NullReferenceException e) { checkMovie = -1; }
                        catch (MySqlException e) { checkMovie = -1; }

                    }

                    /*int[] idActors = new int[actors.Length];
                    int[] idDirectors = new int[directors.Length];
                    int[] idWriters = new int[writers.Length];
                    int[] idLanguages = new int[languages.Length];*/

                    foreach (int i in idActors)
                    {
                        using (var add_filmactor_cmd = new MySqlCommand("INSERT INTO film_actor(id_film,id_actor) values(@id_film,@id_actor)",
                      conn))
                        {
                            add_filmactor_cmd.Parameters.Add("@id_film", MySqlDbType.Int16).Value = checkMovie;
                            add_filmactor_cmd.Parameters.Add("@id_actor", MySqlDbType.Int16).Value = i;
                            add_filmactor_cmd.ExecuteNonQuery();
                        }
                    }
                    foreach (int i in idDirectors)
                    {
                        using (var add_filmdirector_cmd = new MySqlCommand("INSERT INTO film_director(id_film,id_director) values(@id_film,@id_director)",
                      conn))
                        {
                            add_filmdirector_cmd.Parameters.Add("@id_film", MySqlDbType.Int16).Value = checkMovie;
                            add_filmdirector_cmd.Parameters.Add("@id_director", MySqlDbType.Int16).Value = i;
                            add_filmdirector_cmd.ExecuteNonQuery();
                        }

                    }
                    foreach (int i in idWriters)
                    {
                        using (var add_filmwriter_cmd = new MySqlCommand("INSERT INTO film_writer(id_film,id_writer) values(@id_film,@id_writer)",
                      conn))
                        {
                            add_filmwriter_cmd.Parameters.Add("@id_film", MySqlDbType.Int16).Value = checkMovie;
                            add_filmwriter_cmd.Parameters.Add("@id_writer", MySqlDbType.Int16).Value = i;
                            add_filmwriter_cmd.ExecuteNonQuery();
                        }

                    }
                    foreach (int i in idCategories)
                    {
                        using (var add_filmcategory_cmd = new MySqlCommand("INSERT INTO film_category(id_film,id_category) values(@id_film,@id_category)",
                      conn))
                        {
                            add_filmcategory_cmd.Parameters.Add("@id_film", MySqlDbType.Int16).Value = checkMovie;
                            add_filmcategory_cmd.Parameters.Add("@id_category", MySqlDbType.Int16).Value = i;
                            add_filmcategory_cmd.ExecuteNonQuery();
                        }

                    }
                    conn.Close();

                    return true;
                }
            }
            else return true;
        }

        public string[] getMovieData(string title, int quality, int year, movieData s)
        {

            /*           SQL queries:
 *  Actors in film: SELECT name FROM `film` JOIN(film_actor) ON (film.id_film=film_actor.id_film) JOIN actor ON actor.id_actor=film_actor.id_actor WHERE film.id_film= group by name
 *  Director in film: SELECT name FROM `film` JOIN(film_director) ON (film.id_film=film_director.id_film) JOIN director ON director.id_director=film_director.id_director WHERE film.id_film= group by name
 *  Writers in film: SELECT name FROM `film` JOIN(film_writer) ON (film.id_film=film_writer.id_film) JOIN writer ON writer.id_writer=film_writer.id_writer WHERE film.id_film= group by name
 *  Film categories: SELECT name FROM `film` JOIN(film_category) ON (film.id_film=film_category.id_film) JOIN category ON category.id_category=film_category.id_category WHERE film.id_film= group by name
 * 
 * 
 * 
 * 
 * */
            DataTable tmp;
            string[] tmp_str = null;
            switch (s)
            {
                case movieData.category:
                    //SELECT name FROM `film` JOIN(film_category) ON (film.id_film=film_category.id_film) JOIN category ON category.id_category=film_category.id_category WHERE film.id_film= group by name

                    tmp = sql_query("SELECT name FROM `film` JOIN(film_category) ON (film.id_film=film_category.id_film) JOIN category ON category.id_category=film_category.id_category WHERE film.title=\'" + title + "\' AND id_quality=" + quality.ToString() + " group by name");
                    tmp_str = new string[tmp.Rows.Count];
                    for (int n = 0; n < tmp.Rows.Count; n++)
                    {
                        tmp_str[n] = tmp.Rows[n][0].ToString();
                    }

                    break;
                case movieData.id:
                    tmp = sql_query("Select id_film from film where title=\'" + title + "\' AND id_quality=" + quality.ToString());
                    tmp_str = new string[tmp.Rows.Count];
                    for (int n = 0; n < tmp.Rows.Count; n++)
                    {
                        tmp_str[n] = tmp.Rows[n][0].ToString();
                    }
                    break;
                case movieData.length:
                    tmp = sql_query("Select length from film where title=\'" + title + "\' AND id_quality=" + quality.ToString());
                    tmp_str = new string[tmp.Rows.Count];
                    for (int n = 0; n < tmp.Rows.Count; n++)
                    {
                        tmp_str[n] = tmp.Rows[n][0].ToString();
                    }

                    break;
                case movieData.plot:
                    tmp = sql_query("Select description from film where title=\'" + title + "\' AND id_quality=" + quality.ToString());
                    tmp_str = new string[tmp.Rows.Count];
                    for (int n = 0; n < tmp.Rows.Count; n++)
                    {
                        tmp_str[n] = tmp.Rows[n][0].ToString();
                    }
                    break;
                case movieData.quality:
                    break;
                case movieData.rating:
                    tmp = sql_query("Select rating from film where title=\'" + title + "\' AND id_quality=" + quality.ToString());
                    tmp_str = new string[tmp.Rows.Count];
                    for (int n = 0; n < tmp.Rows.Count; n++)
                    {
                        tmp_str[n] = tmp.Rows[n][0].ToString();
                    }
                    break;
                case movieData.subtitles:
                    tmp = sql_query("Select subtitles from film where title=\'" + title + "\' AND id_quality=" + quality.ToString());
                    tmp_str = new string[tmp.Rows.Count];
                    for (int n = 0; n < tmp.Rows.Count; n++)
                    {
                        tmp_str[n] = tmp.Rows[n][0].ToString();
                    }
                    break;
                case movieData.title:
                    break;
                case movieData.year:
                    tmp = sql_query("Select year from film where title=\'" + title + "\' AND id_quality=" + quality.ToString());
                    tmp_str = new string[tmp.Rows.Count];
                    for (int n = 0; n < tmp.Rows.Count; n++)
                    {
                        tmp_str[n] = tmp.Rows[n][0].ToString();
                    }
                    break;
                case movieData.writers:
                    tmp = sql_query("SELECT name FROM `film` JOIN(film_writer) ON (film.id_film=film_writer.id_film) JOIN writer ON writer.id_writer=film_writer.id_writer where title=\'" + title + "\' AND id_quality=" + quality.ToString() + " group by name");
                    tmp_str = new string[tmp.Rows.Count];
                    for (int n = 0; n < tmp.Rows.Count; n++)
                    {
                        tmp_str[n] = tmp.Rows[n][0].ToString();
                    }
                    break;
                case movieData.directors:
                    tmp = sql_query("SELECT name FROM `film` JOIN(film_director) ON (film.id_film=film_director.id_film) JOIN director ON director.id_director=film_director.id_director WHERE title=\'" + title + "\' AND id_quality=" + quality.ToString() + " group by name");
                    tmp_str = new string[tmp.Rows.Count];
                    for (int n = 0; n < tmp.Rows.Count; n++)
                    {
                        tmp_str[n] = tmp.Rows[n][0].ToString();
                    }
                    break;
                case movieData.actors:
                    tmp = sql_query("SELECT name FROM `film` JOIN(film_actor) ON (film.id_film=film_actor.id_film) JOIN actor ON actor.id_actor=film_actor.id_actor WHERE title=\'" + title + "\' AND id_quality=" + quality.ToString() + " group by name");
                    tmp_str = new string[tmp.Rows.Count];
                    for (int n = 0; n < tmp.Rows.Count; n++)
                    {
                        tmp_str[n] = tmp.Rows[n][0].ToString();
                    }
                    break;

            }
            return tmp_str;
        }

        public int actorCheck(string actorIn)
        {
            openConnection();
            int actorId = -1;
            using (var checkId_cmd = new MySqlCommand("SELECT id_actor from actor where name=@actorInput",
                                  conn))
            {
                checkId_cmd.Parameters.Add("@actorInput", MySqlDbType.VarChar).Value = actorIn;
                try { actorId = (int)checkId_cmd.ExecuteScalar(); }
                catch (NullReferenceException e) { actorId = -1; }

            }

            if (actorId == -1)
            {
                using (var add_actor_cmd = new MySqlCommand("INSERT INTO actor(name) values(@name)",
                                  conn))
                {
                    add_actor_cmd.Parameters.Add("@name", MySqlDbType.VarChar).Value = actorIn;
                    add_actor_cmd.ExecuteNonQuery();
                }

                using (var checkId_cmd = new MySqlCommand("SELECT id_actor from actor where name=@actorInput",
                                  conn))
                {
                    checkId_cmd.Parameters.Add("@actorInput", MySqlDbType.VarChar).Value = actorIn;
                    try { actorId = (int)checkId_cmd.ExecuteScalar(); }
                    catch (NullReferenceException e) { actorId = -1; }

                }
            }
            conn.Close();
            return actorId;

        }

        public int writerCheck(string writerIn)
        {
            openConnection();
            int writerId = -1;
            using (var checkId_cmd = new MySqlCommand("SELECT id_writer from writer where name=@writerInput",
                                  conn))
            {
                checkId_cmd.Parameters.Add("@writerInput", MySqlDbType.VarChar).Value = writerIn;
                try { writerId = (int)checkId_cmd.ExecuteScalar(); }
                catch (NullReferenceException e) { writerId = -1; }

            }

            if (writerId == -1)
            {
                using (var add_writer_cmd = new MySqlCommand("INSERT INTO writer(name) values(@name)",
                                 conn))
                {
                    add_writer_cmd.Parameters.Add("@name", MySqlDbType.VarChar).Value = writerIn;
                    add_writer_cmd.ExecuteNonQuery();
                }

                using (var checkId_cmd = new MySqlCommand("SELECT id_writer from writer where name=@writerInput",
                                  conn))
                {
                    checkId_cmd.Parameters.Add("@writerInput", MySqlDbType.VarChar).Value = writerIn;
                    try { writerId = (int)checkId_cmd.ExecuteScalar(); }
                    catch (NullReferenceException e) { writerId = -1; }

                }
            }
            conn.Close();
            return writerId;

        }

        public int directorCheck(string directorIn)
        {
            openConnection();
            int directorId = -1;
            using (var checkId_cmd = new MySqlCommand("SELECT id_director from director where name=@directorInput",
                                  conn))
            {
                checkId_cmd.Parameters.Add("@directorInput", MySqlDbType.VarChar).Value = directorIn;
                try { directorId = (int)checkId_cmd.ExecuteScalar(); }
                catch (NullReferenceException e) { directorId = -1; }

            }

            if (directorId == -1)
            {
                using (var add_director_cmd = new MySqlCommand("INSERT INTO director(name) values(@name)",
                                 conn))
                {
                    add_director_cmd.Parameters.Add("@name", MySqlDbType.VarChar).Value = directorIn;
                    add_director_cmd.ExecuteNonQuery();
                }

                using (var checkId_cmd = new MySqlCommand("SELECT id_director from director where name=@directorInput",
                                  conn))
                {
                    checkId_cmd.Parameters.Add("@directorInput", MySqlDbType.VarChar).Value = directorIn;
                    try { directorId = (int)checkId_cmd.ExecuteScalar(); }
                    catch (NullReferenceException e) { directorId = -1; }

                }
            }
            conn.Close();
            return directorId;

        }

        public int categoryCheck(string categoryIn)
        {
            openConnection();
            int categoryId = -1;
            using (var checkId_cmd = new MySqlCommand("SELECT id_category from category where name=@categoryInput",
                                  conn))
            {
                checkId_cmd.Parameters.Add("@categoryInput", MySqlDbType.VarChar).Value = categoryIn;
                try { categoryId = (int)checkId_cmd.ExecuteScalar(); }
                catch (NullReferenceException e) { categoryId = -1; }

            }

            if (categoryId == -1)
            {
                using (var add_category_cmd = new MySqlCommand("INSERT INTO category(name) values(@name)",
                                 conn))
                {
                    add_category_cmd.Parameters.Add("@name", MySqlDbType.VarChar).Value = categoryIn;
                    add_category_cmd.ExecuteNonQuery();
                }

                using (var checkId_cmd = new MySqlCommand("SELECT id_category from category where name=@categoryInput",
                                 conn))
                {
                    checkId_cmd.Parameters.Add("@categoryInput", MySqlDbType.VarChar).Value = categoryIn;
                    try { categoryId = (int)checkId_cmd.ExecuteScalar(); }
                    catch (NullReferenceException e) { categoryId = -1; }

                }
            }
            conn.Close();
            return categoryId;

        }

        public int languageCheck(string languageIn)
        {
            openConnection();
            int languageId = -1;
            using (var checkId_cmd = new MySqlCommand("SELECT id_language from language where name=@languageInput",
                                  conn))
            {
                checkId_cmd.Parameters.Add("@languageInput", MySqlDbType.VarChar).Value = languageIn;
                try { languageId = (int)checkId_cmd.ExecuteScalar(); }
                catch (NullReferenceException e) { languageId = -1; }

            }

            if (languageId == -1)
            {

                using (var add_language_cmd = new MySqlCommand("INSERT INTO language(name) values(@name)",
                                  conn))
                {
                    add_language_cmd.Parameters.Add("@name", MySqlDbType.VarChar).Value = languageIn;
                    add_language_cmd.ExecuteNonQuery();
                }

                using (var checkId_cmd = new MySqlCommand("SELECT id_language from language where name=@languageInput",
                                 conn))
                {
                    checkId_cmd.Parameters.Add("@languageInput", MySqlDbType.VarChar).Value = languageIn;
                    try { languageId = (int)checkId_cmd.ExecuteScalar(); }
                    catch (NullReferenceException e) { languageId = -1; }

                }
            }
            conn.Close();
            return languageId;

        }

        /// <summary>
        /// Checks if RFID code is linked to any customer in database
        /// Returns -1 if ID is not found
        /// </summary>
        /// <param name="inputId">RFID code</param>
        /// <returns>Customer id if found.
        ///         -1 if not found.</returns>
        public int checkID(string inputId)
        {
            openConnection();
            using (var checkId_cmd = new MySqlCommand("SELECT id_customer from customer where rfid=@rfidInput",
                               conn))
            {
                checkId_cmd.Parameters.Add("@rfidInput", MySqlDbType.VarChar).Value = inputId;
                int n;
                try { n = (int)checkId_cmd.ExecuteScalar(); }
                catch (NullReferenceException e) { n = -1; }
                //RBBA29F02300000000=00001T1
                conn.Close();
                // Returns -1 if id not found
                return n;
            }
        }

        public Byte[] get_posterFromDB(int idFilm)
        {
            MySqlDataReader Reader;
            using (var cmd = new MySqlCommand("SELECT poster FROM film WHERE id_film=@filmID",
                                  conn))
            {
                openConnection();

                cmd.Parameters.Add("@filmID", MySqlDbType.Int16).Value = idFilm;
                Reader = cmd.ExecuteReader();

                if (Reader.Read())
                {
                    Byte[] imgdata = Reader.GetValue(0) as Byte[];
                    conn.Close();
                    return imgdata;
                }
                else
                {
                    return null;
                }
            }
        }

        private Byte[] get_posterFromURI(string uriIn)
        {
            byte[] arr;

            using (WebClient client = new WebClient())
            {
                arr = client.DownloadData(uriIn);
            }

            return arr;
        }


        private void openConnection()
        {

            try
            {
                conn.Open();
            }

            catch (System.InvalidOperationException e) { }
            catch (MySql.Data.MySqlClient.MySqlException e)
            {
                { MessageBox.Show(e.ToString(), "MySql Exception", MessageBoxButton.OK, MessageBoxImage.Error); }
            }
        }

        private string[] csv_parser(string data)
        {

            /*Parses comma separated data to string array 
            
             *             "Tim Roth, Amanda Plummer, Laura Lovelace, John Travolta"
                    ->          [0]Tim Roth
             *                  [1]Amanda Plummer
             *                  [2]Laura Lovelace
             *                  [3]John Travolta
             *                  */

            int start = -1;
            int m = 0;
            int l = 0;
            int objCount = 1;

            for (int i = 0; i < data.Length; i++) if (data[i] == ',') objCount++;

            String[] dataParsed = new String[objCount];

            for (int i = 0; i < data.Length; i++)
            {
                if (data[i] == '(')
                {
                    if (data[i - 1] == ' ')
                    {
                        data = data.Remove(i - 1, 1);
                        i = i - 1;
                    }
                    for (int j = i; j < data.Length; j++)
                    {
                        if (data[j] == ')')
                        {
                            data = data.Remove(i, (j - i + 1));
                            if (i == data.Length) i = i - 1;
                            break;
                        }
                    }
                }
                if (data[i] == ',')
                {
                    if (data[i + 1] == ' ')
                    {
                        data = data.Remove(i + 1, 1);
                    }
                    //  if (m != 0) l = start + 2;
                    // else 
                    l = start + 1;
                    for (int j = l; j < i; j++)
                    {
                        dataParsed[m] += data[j];
                    }
                    start = i;
                    m++;
                }

            }
            l = start + 1;
            for (int j = l; j < data.Length; j++)
            {
                dataParsed[m] += data[j];
            }
            return dataParsed;
        }

        public DataTable sql_query(string command)
        {
            openConnection();
            DataTable dTable = new DataTable();
            try
            {
                using (var cmd = new MySqlCommand(command,
                                      conn))
                {
                    MySqlDataReader dReader = cmd.ExecuteReader();
                    dTable.Load(dReader);
                    dReader.Close();
                }
            }
            catch (MySqlException) { }
            finally
            {
                conn.Close();

            }
            return dTable;
        }
    }
}
