﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RFID
{
    /// <summary>
    /// Interaction logic for istumapaikka.xaml
    /// </summary>
    public partial class istumapaikka : Window
    {
        public int valinta = 0;
        int[] rowCol = new int[2];

        List<Image> istumapaikatLista = new List<Image>();

        public static Boolean RFIDtunnistettu = true;        //epätosi kunnes käyttäjä tunnistetaan

        public istumapaikka()
        {
            InitializeComponent();
            listSeats();
            button_ok.IsEnabled = false;
            istuinten_nollaus();

            if (istumapaikka.RFIDtunnistettu == false)                   //"Käyttäjä tunnistettu"-teksti on piilossa
            {
                istumapaikka_RFID_tunnistettu.Visibility = Visibility.Hidden;

                käyttäjän_nimi.Visibility = Visibility.Hidden;
            }
            else if (istumapaikka.RFIDtunnistettu == true)                           //Vaihetaan tekstejä, kun käyttäjän on tunnistettu
            {
                istumapaikka_RFID_tunnistettu.Visibility = Visibility.Visible;
                istumapaikka_RFID_ei_tunnistettu.Visibility = Visibility.Hidden;

                käyttäjän_nimi.Text = MainWindow.kayttajan_nimi;                   //Muutetaan textblockin tekstiä
                käyttäjän_nimi.Visibility = Visibility.Visible;
            }
        }

        private void paikan_valinta(object sender, MouseButtonEventArgs e)
        {
            string sndTmp = ((TextBlock)sender).Name.ToString();
            
            int.TryParse(sndTmp[1].ToString(), out rowCol[0]);
            if(sndTmp.Length==4) int.TryParse(sndTmp[3].ToString(), out rowCol[1]);
            else int.TryParse((sndTmp[3].ToString()+sndTmp[4].ToString()), out rowCol[1]);

            istuinten_nollaus();
            foreach (Image a in istumapaikatLista)
            {//"Rivi1_Paikka1"
                
                if (a.Name == "Rivi" + rowCol[0].ToString() + "_" + "Paikka" + rowCol[1].ToString())
                {
                    MainWindow.rivi_number = rowCol[0];                 //rivin numero tallennetaan, jotta sitä voidaan käyttää myöhemmin 
                    MainWindow.istuin_number = rowCol[1];               //istuimen numero tallennetaan, jotta sitä voidaan käyttää myöhemmin       
                    a.Visibility = Visibility.Visible;
                    button_ok.IsEnabled = true;
                    break;
                }
            }
        }


        private void istuinten_nollaus()
        {
            foreach (Image a in istumapaikatLista)
            {
                a.Visibility = Visibility.Hidden;
            }
            
        }

        private void button_takaisin_click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void button_ok_click(object sender, RoutedEventArgs e)
        {
           // if (rowCol[0] == 0 || rowCol[1] == 0) //Ei valittua istuinta
 
            //ostoikkuna newWindow = new ostoikkuna();
            //newWindow.Show();
            ostoikkuna newWindow = new ostoikkuna();
            newWindow.Show();
        }


        /*************************************************
         * 
         * Istumapaikkojen lisäys tietokantaan
         * 
         * string tmp;
            int[] tmpInt=new int[2];
            foreach (Image a in istumapaikatLista)
            {
                tmp = a.Name.ToString();
                int.TryParse(tmp[4].ToString(), out tmpInt[0]);
                if (tmp.Length == 13) int.TryParse(tmp[12].ToString(), out tmpInt[1]);
                else int.TryParse((tmp[12].ToString() + tmp[13].ToString()), out tmpInt[1]);
                sqlConn.insert_seats(1, tmpInt);
            }
         * ****************************************************/

        private void listSeats()
        {
            istumapaikatLista.Add(Rivi1_Paikka1);
            istumapaikatLista.Add(Rivi1_Paikka2);
            istumapaikatLista.Add(Rivi1_Paikka3);
            istumapaikatLista.Add(Rivi1_Paikka4);
            istumapaikatLista.Add(Rivi1_Paikka5);
            istumapaikatLista.Add(Rivi1_Paikka6);
            istumapaikatLista.Add(Rivi1_Paikka7);
            istumapaikatLista.Add(Rivi1_Paikka8);

            istumapaikatLista.Add(Rivi2_Paikka1);
            istumapaikatLista.Add(Rivi2_Paikka2);
            istumapaikatLista.Add(Rivi2_Paikka3);
            istumapaikatLista.Add(Rivi2_Paikka4);
            istumapaikatLista.Add(Rivi2_Paikka5);
            istumapaikatLista.Add(Rivi2_Paikka6);
            istumapaikatLista.Add(Rivi2_Paikka7);
            istumapaikatLista.Add(Rivi2_Paikka8);
            istumapaikatLista.Add(Rivi2_Paikka9);

            istumapaikatLista.Add(Rivi3_Paikka1);
            istumapaikatLista.Add(Rivi3_Paikka2);
            istumapaikatLista.Add(Rivi3_Paikka3);
            istumapaikatLista.Add(Rivi3_Paikka4);
            istumapaikatLista.Add(Rivi3_Paikka5);
            istumapaikatLista.Add(Rivi3_Paikka6);
            istumapaikatLista.Add(Rivi3_Paikka7);
            istumapaikatLista.Add(Rivi3_Paikka8);
            istumapaikatLista.Add(Rivi3_Paikka9);
            istumapaikatLista.Add(Rivi3_Paikka10);

            istumapaikatLista.Add(Rivi4_Paikka1);
            istumapaikatLista.Add(Rivi4_Paikka2);
            istumapaikatLista.Add(Rivi4_Paikka3);
            istumapaikatLista.Add(Rivi4_Paikka4);
            istumapaikatLista.Add(Rivi4_Paikka5);
            istumapaikatLista.Add(Rivi4_Paikka6);
            istumapaikatLista.Add(Rivi4_Paikka7);
            istumapaikatLista.Add(Rivi4_Paikka8);
            istumapaikatLista.Add(Rivi4_Paikka9);
            istumapaikatLista.Add(Rivi4_Paikka10);

            istumapaikatLista.Add(Rivi5_Paikka1);
            istumapaikatLista.Add(Rivi5_Paikka2);
            istumapaikatLista.Add(Rivi5_Paikka3);
            istumapaikatLista.Add(Rivi5_Paikka4);
            istumapaikatLista.Add(Rivi5_Paikka5);
            istumapaikatLista.Add(Rivi5_Paikka6);
            istumapaikatLista.Add(Rivi5_Paikka7);
            istumapaikatLista.Add(Rivi5_Paikka8);
            istumapaikatLista.Add(Rivi5_Paikka9);
            istumapaikatLista.Add(Rivi5_Paikka10);

            istumapaikatLista.Add(Rivi6_Paikka1);
            istumapaikatLista.Add(Rivi6_Paikka2);
            istumapaikatLista.Add(Rivi6_Paikka3);
            istumapaikatLista.Add(Rivi6_Paikka4);
            istumapaikatLista.Add(Rivi6_Paikka5);
            istumapaikatLista.Add(Rivi6_Paikka6);
            istumapaikatLista.Add(Rivi6_Paikka7);
            istumapaikatLista.Add(Rivi6_Paikka8);
            istumapaikatLista.Add(Rivi6_Paikka9);
            istumapaikatLista.Add(Rivi6_Paikka10);

            istumapaikatLista.Add(Rivi7_Paikka1);
            istumapaikatLista.Add(Rivi7_Paikka2);
            istumapaikatLista.Add(Rivi7_Paikka3);
            istumapaikatLista.Add(Rivi7_Paikka4);
            istumapaikatLista.Add(Rivi7_Paikka5);
            istumapaikatLista.Add(Rivi7_Paikka6);
            istumapaikatLista.Add(Rivi7_Paikka7);
            istumapaikatLista.Add(Rivi7_Paikka8);
            istumapaikatLista.Add(Rivi7_Paikka9);
            istumapaikatLista.Add(Rivi7_Paikka10);
            istumapaikatLista.Add(Rivi7_Paikka11);
            istumapaikatLista.Add(Rivi7_Paikka12);

            istumapaikatLista.Add(Rivi8_Paikka1);
            istumapaikatLista.Add(Rivi8_Paikka2);
            istumapaikatLista.Add(Rivi8_Paikka3);
            istumapaikatLista.Add(Rivi8_Paikka4);
            istumapaikatLista.Add(Rivi8_Paikka5);
            istumapaikatLista.Add(Rivi8_Paikka6);
            istumapaikatLista.Add(Rivi8_Paikka7);
            istumapaikatLista.Add(Rivi8_Paikka8);
            istumapaikatLista.Add(Rivi8_Paikka9);
            istumapaikatLista.Add(Rivi8_Paikka10);
            istumapaikatLista.Add(Rivi8_Paikka11);
            istumapaikatLista.Add(Rivi8_Paikka12);
            istumapaikatLista.Add(Rivi8_Paikka13);
            istumapaikatLista.Add(Rivi8_Paikka14);
        }

    }
}
