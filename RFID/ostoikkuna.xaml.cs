﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RFID
{
    /// <summary>
    /// Interaction logic for ostoikkuna.xaml
    /// </summary>
    public partial class ostoikkuna : Window
    {

        public ostoikkuna()
        {
            InitializeComponent();
            ostonappula.IsEnabled = false;                  //Ostonappula on pois läytöstä kunnes ostaja on tunnistettu


            string rivi_num = MainWindow.rivi_number.ToString();          //muutetaan integer -> string
            rivi_numero.Text = rivi_num;                //asetetaan luku textblockiin

            string istuin_num = MainWindow.istuin_number.ToString();          //muutetaan integer -> string
            istuin_numero.Text = istuin_num;                //asetetaan luku textblockiin

            elokuvan_nimi.Text = MainWindow.elokuva_name;                   //muutetaan textblockia

            string hinta_num = MainWindow.hinta_double.ToString();          //muutetaan double -> string
            hinta_luku.Text = hinta_num;

            double saldo = 4.3;                               //float muuttuja johon SQL komennolla saatu numero voidaan asettaa
            string saldo_num = saldo.ToString();          //muutetaan float -> string
            saldo_luku.Text = saldo_num;


            if (istumapaikka.RFIDtunnistettu == false)                   //"Käyttäjä tunnistettu"-teksti on piilossa
            {
                RFID_tunnistettu.Visibility = Visibility.Hidden;
                käyttäjän_nimi.Visibility = Visibility.Hidden;
            }
            else if (istumapaikka.RFIDtunnistettu == true)                           //Vaihetaan tekstejä, kun käyttäjän on tunnistettu
            {
                RFID_tunnistettu.Visibility = Visibility.Visible;
                RFID_ei_tunnistettu.Visibility = Visibility.Hidden;

                käyttäjän_nimi.Text = MainWindow.kayttajan_nimi;                   //muutetaan textblockia
                käyttäjän_nimi.Visibility = Visibility.Visible;

                if (saldo > MainWindow.hinta_double)
                {
                    ostonappula.IsEnabled = true;                           //Nappula käyttöön, jos saldo > hinta
                }

            }
        }


        private void Window_Deactivated_1(object sender, EventArgs e)
        {
            //this.Close();
        }

        private void ostalippu(object sender, RoutedEventArgs e)
        {
            lipun_tulostus newWindow = new lipun_tulostus();
            newWindow.Show();
        }

        private void takaisin_click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }


    }
}
