﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RFID
{
    /// <summary>
    /// Interaction logic for elokuvan_tiedot.xaml
    /// </summary>
    public partial class elokuvan_tiedot : Window
    {
        public elokuvan_tiedot(int n, string title, string plot, string year, string[] director, string[] writers, string[] actors, string rating)
        {
            InitializeComponent();
            switch (n)
            {
                case 1:
                    elokuvan_tiedot2.Text += "\t\t" + title + "  (" + year + ")" + "\nIMDb rating: " + rating;
                    elokuvan_tiedot2.Text += "\n\n";
                    elokuvan_tiedot2.Text += "Plot";
                    elokuvan_tiedot2.Text += "\n\n";
                    elokuvan_tiedot2.Text += plot;
                    elokuvan_tiedot2.Text += "\n\nDirector(s):\n\t";
                    foreach (string s in director)
                    {
                        elokuvan_tiedot2.Text += s + "\n\t";
                    }
                    elokuvan_tiedot2.Text += "\nWriter(s):\n\t";
                    foreach (string s in writers)
                    {
                        elokuvan_tiedot2.Text += s + "\n\t";
                    }
                    elokuvan_tiedot2.Text += "\nActors:\n\t";
                    foreach (string s in actors)
                    {
                        elokuvan_tiedot2.Text += s + "\n\t";
                    }
                    break;
                case 2:
                    elokuvan_tiedot2.Text = "Elokuvan 2 tiedot";
                    break;
                case 3:
                    elokuvan_tiedot2.Text = "Elokuvan 3 tiedot";
                    break;
                case 4:
                    elokuvan_tiedot2.Text = "Elokuvan 4 tiedot";
                    break;
                case 5:
                    elokuvan_tiedot2.Text = "Elokuvan 5 tiedot";
                    
                    break;
                case 6:
                    elokuvan_tiedot2.Text = "Elokuvan 6 tiedot";
                    break;
            }
        }

        private void elokuvan_tiedot_click(object sender, MouseButtonEventArgs e)
        {
            //this.Close();
        }

        private void Window_Deactivated(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
