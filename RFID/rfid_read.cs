﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Windows;
using System.Text;
using System.Threading.Tasks;
using System.IO.Ports;
using System.Threading;

namespace RFID
{

    /*RBBA29F2E300000000=00001TF
RBC5FD267E00000000=00001T3
RB BA 29 F2 E3 00 00 00 00 =00001T F

RBC602D4CE00000000=00001T6
     * 
RB CA 60 35 9B 00 00 00 00 =0000 1T1
RB BA 29 F0 23 00 00 00 00 =0000 1T1
     * 
     01 23456789abcdefgh ijklm n o p  <----- merkkisapluuna

Merkkijonon rakenne
01     : aina samoja
2 - h  : 16 merkkiä, sisältävät sirun tunnisteen (= avain tietokantakyselyyn)
i - m  : aina samoja
n    : tunnistimen tyyppi, nyt Mifare
o    : lopetusmerkki
p    : tarkistussumma
     */
public delegate void rfidInputHandler(string rfidData);
    class rfid_read
    {
/*
 * 
 * Reader status query="Q";
Infrared sensor: Not equipped as a default
Battery level: Indicate back- up capacitors charge level
External power level:
• 1 is equivalent to 0.1 V
• i.e. value 0x9B ~ 155 (dec) ~ 15.5V
Input status bits: XXABXXXT
• A:input A (1,0)
• B: input B (1,0)
• T: tamper (1,0)
• X: RFU
ISL sensor: Not equipped as a default
Temperature sensor:
• answer is signed char
• temperature scale is Celsius
• 00 = 0 ºC
• 7F = 128 ºC
• 80 = -128 ºC 
*/
        public SerialPort serialPort1;
        public string RxString;
        public string rfid_Input;

        public event rfidInputHandler newRfidData;

        public rfid_read()
        {
            try { 
                    //serialPort1 = new SerialPort("COM3", 9600);
                    serialPort1 = new SerialPort("COM4", 9600);

                    serialPort1.Open();
                    //serialPort1.Write("Q");//Status query
                }
            catch (System.IO.IOException e) { MessageBox.Show(e.ToString(),"IO Exception",MessageBoxButton.OK,MessageBoxImage.Error); }
            serialPort1.DataReceived += new SerialDataReceivedEventHandler(serialPort1_DataReceived);
        }

        public void serialPort1_DataReceived(object sender, System.IO.Ports.SerialDataReceivedEventArgs e)
        {
            

            RxString = serialPort1.ReadExisting();
            if (RxString.StartsWith("R")) rfid_Input = RxString;

            else rfid_Input += RxString;
            if (rfid_Input.Length > 26) rfid_Input = rfid_Input.Remove(26);
            if (rfid_Input.Length==26)
            {
                if (rfid_Input.StartsWith("RB"))
                {   
                    newRfidData(rfid_Input);
                    
                }
            }

        }
    }
}
