﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data;
using System.Linq;
using System.Threading.Tasks;
using System.Windows;
using System.Globalization;
using System.IO;
using System.Threading;

namespace RFID
{
    /// <summary>
    /// Interaction logic for App.xaml
    /// </summary>
    public partial class App : Application
    {
        public static App Instance;
        public static String Directory;
        public event EventHandler LanguageChangedEvent;
        public App()
        {
            Instance = this;
            Directory = System.IO.Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location)+"\\lang\\";
            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");
            // Load the Localization Resource Dictionary based on OS language
            //SetLanguageResourceDictionary(GetLocXAMLFilePath(CultureInfo.CurrentCulture.Name));
        }
        public void SwitchLanguage(string inFiveCharLang)
        {
           //if (CultureInfo.CurrentCulture.Name.Equals(inFiveCharLang))
             //   return;

            var ci = new CultureInfo(inFiveCharLang);
            Thread.CurrentThread.CurrentCulture = ci;
            Thread.CurrentThread.CurrentUICulture = ci;
            
            
            SetLanguageResourceDictionary(GetLocXAMLFilePath(inFiveCharLang));
            if (null != LanguageChangedEvent)
            {
                LanguageChangedEvent(this, new EventArgs());
            }
        }

        private string GetLocXAMLFilePath(string inFiveCharLang)
        {
            string locXamlFile = "lang." + inFiveCharLang + ".xaml";
            return Path.Combine(Directory, inFiveCharLang, locXamlFile);
        }

        /// <summary>
        /// Sets or replaces the ResourceDictionary by dynamically loading
        /// a Localization ResourceDictionary from the file path passed in.
        /// </summary>
        /// <param name="inFile"></param>
        private void SetLanguageResourceDictionary(String inFile)
        {
            // Read in ResourceDictionary File
            var languageDictionary = new ResourceDictionary();
            //if (File.Exists(inFile))
           // {
                try { languageDictionary.Source = new Uri(inFile); }
                catch (IOException) { return; }
                catch (System.ArgumentException) { return; }


                // Remove any previous Localization dictionaries loaded
                int langDictId = -1;
                for (int i = 0; i < Resources.MergedDictionaries.Count; i++)
                {
                    var md = Resources.MergedDictionaries[i];
                    // Make sure your Localization ResourceDictionarys have the ResourceDictionaryName
                    // key and that it is set to a value starting with "Loc-".
                    if (md.Contains("ResourceDictionaryName"))
                    {

                        if (md["ResourceDictionaryName"].ToString().StartsWith("Loc-"))
                        {
                            langDictId = i;
                            break;
                        }
                    }
                }

                if (langDictId == -1)
                {
                    // Add in newly loaded Resource Dictionary
                    Resources.MergedDictionaries.Add(languageDictionary);
                }
                else
                {
                    // Replace the current langage dictionary with the new one
                    Resources.MergedDictionaries[langDictId] = languageDictionary;
                }
            }
        }
        
    }



