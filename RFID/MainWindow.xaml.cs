﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.ComponentModel;
using System.Resources;
using System.Threading;
using System.Globalization;
using System.Data;
using System.IO;

namespace RFID
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public static int ikkuna = 1;
        public static int button = 0;

        public static string elokuva_name = "Pulp Fiction";         //SQL komennolla saatu nimi asetetaan tähän muuttujaan
        public static int istuin_number = 5;                               //integer muuttuja johon SQL komennolla saatu numero voidaan asettaa
        public static int rivi_number = 3;                               //integer muuttuja johon SQL komennolla saatu numero voidaan asettaa
        public static double hinta_double = 1.5;                               //double muuttuja johon SQL komennolla saatu numero voidaan asettaa
        public static string kayttajan_nimi = "Markus Varila";             //Muuttuja johon voidaan liittää SQL komennolla saatu tieto. Muuttuja näkyy myös muihin ikkunoihin
        //Päivämäärä muuttujat
        public static int dd = 12;
        public static int mm = 10;
        public static int yyyy = 2015;
        public static int hh = 18;

        public static string[] customerData;

        sql sqlConnection = null;
        rfid_read RfidReader = null;
        omdb_api omdbApi;

        public MainWindow()
        {
            RFID.App.Instance.SwitchLanguage("en-US");

            InitializeComponent();

            RfidReader = new rfid_read();
            RfidReader.newRfidData += new rfidInputHandler(rfidCheck);
            omdbApi = new omdb_api();

            Thread.CurrentThread.CurrentCulture = new CultureInfo("en-US");

            sqlConnection = new sql("mysli.oamk.fi", "t3oljo00", "6yjZqbosZnrFFFjh", "opisk_t3oljo00");


            DataTable tmpDt = sqlConnection.sql_query("SELECT Count(*) FROM customer"); //Check how many columns there are
            string tmp_str = tmpDt.Rows[0][0].ToString();
            int tmpInt = 0;
            int.TryParse(tmp_str, out tmpInt);

            pvm_valinta.DisplayDateStart = DateTime.Now;
            pvm_valinta.SelectedDate = DateTime.Now;
            customerData = new string[tmpInt];
            rfidCheck("RBBA29F02300000000=00001T1"); //just for test
        
        }
        public void rfidCheck(string rfidIn)
        {
            //txt.Text = rfidIn;
            int customerId = -1;
            if (sqlConnection != null) customerId = sqlConnection.checkID(rfidIn);
            //RBBA29F02300000000=00001T1
            
                if (customerId != -1) //-> käyttäjä tunnistettu
                {

                    istumapaikka.RFIDtunnistettu = true;
                    //DataTable dT = 
                    DataTable tmpDt = sqlConnection.sql_query("SELECT * FROM customer WHERE id_customer=" + customerId);
                    //MessageBox.Show("Customer found!.\nEmail: " + tmpDt.Rows[0][]);
                    for (int n = 0; n < tmpDt.Columns.Count; n++)
                    {
                        customerData[n] = tmpDt.Rows[0][n].ToString();
                    }
                }
                else
                {
                    MessageBox.Show("ID not found in database!", "Invalid RFID", MessageBoxButton.OK, MessageBoxImage.Error);
                    istumapaikka.RFIDtunnistettu = false;
                }
        }

        private void TextBox_TextChanged(object sender, TextChangedEventArgs e)
        {

        }


        private void vasen_reuna_click(object sender, MouseButtonEventArgs e)
        {
            if (ikkuna == 2)
            {
                TabItem1.IsSelected = true;
                ikkuna--;
            }
            else if (ikkuna == 3)
            {
                TabItem2.IsSelected = true;
                ikkuna--;
            }
        }

        private void oikea_reuna_click(object sender, MouseButtonEventArgs e)
        {
            //Ikkuna2 newWindow = new Ikkuna2();
            //newWindow.Show();
            //this.Close();
            if (ikkuna == 1)
            {
                TabItem2.IsSelected = true;
                ikkuna++;
            }
            else if (ikkuna == 2)
            {
                TabItem3.IsSelected = true;
                ikkuna++;
            }
        }

        //Avaa elokuvan tiedot
        private void movieClick(object sender, MouseButtonEventArgs e)
        {
            string s = FindResource("btnBuyTicket").ToString();
            int elokuva = 0;
            string tmp = ((TextBlock)sender).Name.ToString();
            tmp = tmp[tmp.Length - 1].ToString();
            int.TryParse(tmp, out elokuva);
            DataTable test = sqlConnection.sql_query("SELECT * FROM film WHERE id_film=23");

            string[] tmp_arr = new string[test.Columns.Count];
            for (int n = 0; n < test.Columns.Count; n++)
            {
                tmp_arr[n] = test.Rows[0][n].ToString();
            }
            string title = "Wild";
            elokuvan_tiedot newWindow = new elokuvan_tiedot(elokuva, title, sqlConnection.getMovieData(title, 1, 0, sql.movieData.plot)[0], sqlConnection.getMovieData(title, 1, 0, sql.movieData.year)[0], sqlConnection.getMovieData(title, 1, 0, sql.movieData.directors), sqlConnection.getMovieData(title, 1, 0, sql.movieData.writers), sqlConnection.getMovieData(title, 1, 0, sql.movieData.actors), sqlConnection.getMovieData(title, 1, 0, sql.movieData.rating)[0]);
            newWindow.Show();

            sqlConnection.insert_movie("8½", 0, 1);
            sqlConnection.insert_movie("The Imitation Game", 0, 1);
            sqlConnection.insert_movie("Wild", 0, 1);

            int filmID;
            int.TryParse(sqlConnection.getMovieData(title, 1, 0, sql.movieData.id)[0], out filmID);
            Byte[] imgData = sqlConnection.get_posterFromDB(filmID);

             BitmapImage imgtest = new BitmapImage();
             MemoryStream strImg = new MemoryStream(imgData);
             imgtest.BeginInit();
             imgtest.StreamSource = strImg;
             imgtest.EndInit();
             kuva1.Source = imgtest;
        }

        

        //Ostonappula avaa istumapaikan valinnan

        private void button_click(object sender, RoutedEventArgs e)
        {
            string tmp = ((Button)sender).Name.ToString();
            tmp = tmp[tmp.Length - 1].ToString();
            int.TryParse(tmp, out button);
            //istumapaikka newWindow = new istumapaikka();
            Paivamaara newWindow = new Paivamaara();
            newWindow.Show();
        }

        public void changeLang()
        {
            string tmp = CultureInfo.CurrentUICulture.ToString();
            CultureInfo cultureInfo;
            switch (tmp)
            {
                case "en-US":

                    cultureInfo = new CultureInfo("en-US");
                    Thread.CurrentThread.CurrentCulture = cultureInfo;
                    imgLang.BeginInit();
                    imgLang.Source = new BitmapImage(new Uri("/img/flag_united_kingdom.png", UriKind.RelativeOrAbsolute));
                    imgLang.EndInit();
                    RFID.App.Instance.SwitchLanguage("fi-FI");
                    break;
                case "fi-FI":

                    cultureInfo = new CultureInfo("fi-FI");
                    Thread.CurrentThread.CurrentCulture = cultureInfo;
                    imgLang.BeginInit();
                    imgLang.Source = new BitmapImage(new Uri("/img/flag_suomi.png", UriKind.RelativeOrAbsolute));
                    imgLang.EndInit();
                    RFID.App.Instance.SwitchLanguage("en-US");
                    break;
            }
           
        }
        private void Image_MouseUp(object sender, TouchEventArgs e)
        {
            changeLang();
        }

        private void Image_MouseUp(object sender, MouseButtonEventArgs e)
        {
            changeLang();
        }

    }
}
