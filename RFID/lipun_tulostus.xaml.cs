﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RFID
{
    /// <summary>
    /// Interaction logic for lipun_tulostus.xaml
    /// </summary>
    public partial class lipun_tulostus : Window
    {
        public lipun_tulostus()
        {
            InitializeComponent();
            elokuva.Text = MainWindow.elokuva_name;                   //muutetaan textblockia

            string rivi_num = MainWindow.rivi_number.ToString();          //muutetaan integer -> string
            rivi.Text = rivi_num;                                           //asetetaan luku textblockiin
            string istuin_num = MainWindow.istuin_number.ToString();          //muutetaan integer -> string
            paikka.Text = istuin_num;                                   //asetetaan luku textblockiin

            string hinta_num = MainWindow.hinta_double.ToString();          //muutetaan double -> string
            hinta.Text = hinta_num;
        }

        private void deactivated(object sender, EventArgs e)            //sulkee kaikki auki olevat ikkunat paitsi main windowta
        {
            App.Current.Windows[3].Close();
            App.Current.Windows[2].Close();
            App.Current.Windows[1].Close();
            this.Close();
        }
    }
}
