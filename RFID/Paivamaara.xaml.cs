﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Shapes;

namespace RFID
{
    /// <summary>
    /// Interaction logic for Paivamaara.xaml
    /// </summary>
    public partial class Paivamaara : Window
    {
        public Paivamaara()
        {
            InitializeComponent();
        }

        private void button_click_takaisin(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

        private void button1_click(object sender, RoutedEventArgs e)
        {
            istumapaikka newWindow = new istumapaikka();
            newWindow.Show();
        }

        private void button2_click(object sender, RoutedEventArgs e)
        {
            istumapaikka newWindow = new istumapaikka();
            newWindow.Show();
        }

        private void button_click3(object sender, RoutedEventArgs e)
        {
            istumapaikka newWindow = new istumapaikka();
            newWindow.Show();
        }
    }
}
