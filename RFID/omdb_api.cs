﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Net;
using System.IO;
using Newtonsoft.Json;
using System.Windows;

namespace RFID
{
    class omdb_api
    {

        public Dictionary<string, object> dataRequest(string title, int year){

            string reqString;
            WebClient client = new WebClient();

            if (year>=1850 && year<=3000) reqString = "http://www.omdbapi.com/?t=" + title + "&y=" + year.ToString() + "&plot=long&r=json";
            else reqString = "http://www.omdbapi.com/?t=" + title + "&y=&plot=short&r=json";
            //pitkä juonikuvaus &plot=full
            //haettava tyyppi &type= movie, series tai episode
            client.Encoding = System.Text.Encoding.UTF8;
            var json = client.DownloadString(reqString);
            /*
            Palauttaa seuraavanlaisen stringin:
            
            {"Title":"Pulp Fiction","Year":"1994",
            "Rated":"R","Released":"14 Oct 1994",
            "Runtime":"154 min","Genre":"Crime, Drama, Thriller",
            "Director":"Quentin Tarantino",
            "Writer":"Quentin Tarantino (story), Roger Avary (story), Quentin Tarantino",
            "Actors":"Tim Roth, Amanda Plummer, Laura Lovelace, John Travolta",
            "Plot":"Jules Winnfield and Vincent Vega are two hitmen who are out to retrieve a suitcase stolen from their employer, mob boss Marsellus Wallace. Wallace has also asked Vincent to take his wife Mia out a few days later when Wallace himself will be out of town. Butch Coolidge is an aging boxer who is paid by Wallace to lose his next fight. The lives of these seemingly unrelated people are woven together comprising of a series of funny, bizarre and uncalled-for incidents.","Language":"English, Spanish, French","Country":"USA","Awards":"Won 1 Oscar. Another 63 wins & 47 nominations.",
            "Poster":"http://ia.media-imdb.com/images/M/MV5BMjE0ODk2NjczOV5BMl5BanBnXkFtZTYwNDQ0NDg4._V1_SX300.jpg",
            "Metascore":"94","imdbRating":"8.9","imdbVotes":"1,039,031","imdbID":"tt0110912","Type":"movie","Response":"True"}            
            
            
            Jos elokuvaa ei löydy:
            {"Response":"False","Error":"Movie not found!"}
            */
            
            var jsonResponse = JsonConvert.DeserializeObject<Dictionary<string,object>>(json);
            if(jsonResponse["Response"].ToString()=="True") return jsonResponse;//["Poster"];//Palauttaa posterin osoitteen
            else return null; //Jos elokuvaa ei löydy
        }
    }
}
