﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data;
using System.Threading.Tasks;
using System.Threading;
using System.Windows.Threading;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Data;
using System.Drawing;
using System.Windows.Documents;
using System.Windows.Input;
using System.Windows.Media;
using System.Windows.Media.Imaging;
using System.Windows.Navigation;
using System.Windows.Shapes;
using System.IO.Ports;
using System.IO;
using System.Net;
using System.ComponentModel;
using MySql.Data.MySqlClient;
using System.Resources;
using Newtonsoft.Json;


namespace RFID
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// </summary>
    public partial class Main_Window : Window
    {
        public string RxString;
        public delegate void UpdateTextCallback(string message);

        sql sqlConnection=null;
        rfid_read RfidReader=null;
        omdb_api omdbApi;

        public Main_Window()
        {
            
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("fi");
            
            InitializeComponent();
            RfidReader = new rfid_read();
            RfidReader.newRfidData += new rfidInputHandler(rfidCheck);
            omdbApi = new omdb_api();

            sqlConnection = new sql("mysli.oamk.fi", "t3oljo00", "6yjZqbosZnrFFFjh", "opisk_t3oljo00");
            
        }

        public void rfidCheck(string rfidIn)
        {
            //txt.Text = rfidIn;
            int customerId=-1;
            if (sqlConnection != null) customerId=sqlConnection.checkID(rfidIn);
            //RBBA29F02300000000=00001T1
            if (customerId != -1)
            {
                //DataTable dT = 
                DataTable tmpDt=sqlConnection.sql_query("SELECT * FROM customer WHERE id_customer=" + customerId);
                //MessageBox.Show("Customer found!.\nEmail: " + tmpDt.Rows[0][]);
                string[] tmp = new string[tmpDt.Columns.Count];
                for(int n=0; n<tmpDt.Columns.Count;n++)
                {
                    tmp[n]=tmpDt.Rows[0][n].ToString();
                }
                /****************************
                 * customer-taulu:
                 * 0    1       2       3       4       5       6       7
                 * id   fname   lname   phonenum email balance  rfid    discount
                 * *************************/
                MessageBox.Show(tmp[1]);


            }
            else MessageBox.Show("ID not found");
           /* txt.Dispatcher.BeginInvoke(new UpdateTextCallback(this.checkID),
                new object[] { rfidIn });*/
        }

        public void mainWindowClosing(object sender, CancelEventArgs e)
        {
            if (RfidReader !=null) RfidReader.serialPort1.Close();
            if(sqlConnection!=null) sqlConnection.conn.Close();
        }

        private void Button_Click(object sender, RoutedEventArgs e)
        {
            rfidCheck("RBC602D4CE00000000=00001T6");
            System.Threading.Thread.CurrentThread.CurrentUICulture = new System.Globalization.CultureInfo("en");
            //button1.Content = Strings.button2;

            //if (!sqlConnection.insert_movie("asdaf", 2999)) MessageBox.Show("Movie not found!");
           /* BitmapImage bitmap = new BitmapImage();
            bitmap.BeginInit();
            var filmData = omdbApi.dataRequest("8½", 0);
            bitmap.UriSource = new Uri((filmData["Poster"].ToString()), UriKind.Absolute);
            bitmap.EndInit();
            byte[] dlArr;
            byte[] arr;
            using (WebClient client = new WebClient())
            {
                arr = client.DownloadData((omdbApi.dataRequest("8½", 0)["Poster"].ToString()));
                sqlConnection.insert_poster(arr,"postertest2");
            }
            dlArr = sqlConnection.get_poster("postertest2");

            BitmapImage imgtest = new BitmapImage();
            MemoryStream strImg = new MemoryStream(dlArr);
            imgtest.BeginInit();
            imgtest.StreamSource = strImg;
            imgtest.EndInit();
            img.Source = imgtest;*/
        }

        private void btnExit_Click(object sender, RoutedEventArgs e)
        {
            this.Close();
        }

    }
}
